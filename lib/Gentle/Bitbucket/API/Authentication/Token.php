<?php

/*
 * This file is part of the bitbucket-api package.
 *
 * (c) Alexandru G. <alex@gentle.ro>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Gentle\Bitbucket\API\Authentication;

use Buzz\Message\RequestInterface;

/**
 * Basic Authentication
 *
 * @author  Paris Kasidiaris    <pariskasidiaris@gmail.com>
 */
class Token implements AuthenticationInterface
{
    /**
     * Token
     * @var string
     */
    private $token;

    /**
     * @param  string $token API Access Token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Set token
     *
     * @access public
     * @param  string $token
     * @return void
     */
    public function setToken($token)
    {
        $this->token = sprintf('%s', $token);
    }

    /**
     * Get current token
     *
     * @access public
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(RequestInterface $request)
    {
        $request->addHeader('Authorization: Token ' . $this->token);

        return $request;
    }
}
