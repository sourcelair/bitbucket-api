# PHP Bitbucket API
[![Build Status](https://travis-ci.org/gentlero/bitbucket-api.png?branch=develop)](https://travis-ci.org/gentlero/bitbucket-api)

Simple Bitbucket API wrapper for PHP >= 5.3.2. This library is not finished and therefor does not implement entire Bitbucket API for the moment.

## Requirements

* PHP >= 5.3 with [cURL](http://php.net/manual/en/book.curl.php) extension,
* [Buzz](https://github.com/kriswallsmith/Buzz) library,
* PHPUnit to run tests. ( _optional_ )

## Getting started

  1. Install dependencies with composer:
  
    ```bash
    $ php composer.phar install
    ```
  2. See `examples/` directory.

## Documentation

See the `docs` directory for more detailed documentation.

## License

`bitbucket-api` is licensed under the MIT License - see the LICENSE file for details

## Contribute

Send pull requests on [Bitbucket](https://bitbucket.org/gentlero/bitbucket-api).

## Credits

- [Alexandru G.](https://bitbucket.org/vimishor)

